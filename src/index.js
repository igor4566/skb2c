import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  res.json({
    hello: 'JS World',
  });
});
function canonize(url) {
  const re = new RegExp('@?(https?:)?(\/\/)?(([a-zA-Z0-9.]*)[^\/]*\/)?@?([a-zA-Z0-9._]*)');
  const username = url.match(re)[5];
  return '@' + username;
}
app.get('/user', (req, res) => {
  const url = req.query.username;
  const fName = canonize(url);
  res.send(fName);
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
