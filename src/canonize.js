export default function canonize(url) {
	const re = new RegExp('@?(https?:)?(\/\/)?((telegram|vk|vkontakte)[^\/]*\/)?([a-zA-Z0-9]*)');
	const username = url.match(re)[5];
	return '@' + username;

}